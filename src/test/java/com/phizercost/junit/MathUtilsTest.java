package com.phizercost.junit;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;
import org.junit.jupiter.api.Assumptions.*;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MathUtilsTest {
    MathUtils mathUtils;
    TestInfo testInfo;
    TestReporter testReporter;

    @BeforeAll
        //static void beforeAllInit(){
    void beforeAllInit() {
        System.out.println("Has to run before all");
    }

    @BeforeEach
    void init(TestInfo testInfo, TestReporter testReporter) {
        this.testInfo = testInfo;
        this.testReporter = testReporter;
        mathUtils = new MathUtils();
        testReporter.publishEntry("Running " + testInfo.getDisplayName() + " with tags " + testInfo.getTags());

    }

    @AfterEach
    void cleanUp() {
        System.out.println("Cleaning up...");
    }

    @Test
    @DisplayName("Testind add method....")
    void add() {

        int expected = 2;
        int actual = mathUtils.add(1, 1);

        assertEquals(expected
                , actual, "The add method expected " + expected + " but has got " + actual);
    }





    @Test
    @Tag("Circle")
    void testCircleArea() {
        //System.out.println();
        assertEquals(314.1592653589793, mathUtils.computeCircleArea(10), "Should return the circle area which is correct");
    }

    @Test
        //@EnabledOnOs(OS.LINUX)
    void testDivide() {
        boolean isServerUp = false;
        //assumeTrue(isServerUp);
        assertThrows(ArithmeticException.class, () -> mathUtils.divide(1, 0), "Divide by zero should throw an exception");
    }

    @Test
    @DisplayName("Add Method with lambdas")
    void testAgain() {
        assertAll(
                () -> assertEquals(100, mathUtils.add(50, 50)),
                () -> assertEquals(-4, mathUtils.add(1, -5)),
                () -> assertEquals(2, mathUtils.add(190990, -190988))

        );
    }

    @Nested
    class AddTest {
        @RepeatedTest(3)
        @DisplayName("Add Positive")
        @Tag("Math")
        void testAddPositive(RepetitionInfo repetitionInfo) {
            System.out.println(repetitionInfo.getCurrentRepetition() + "," + repetitionInfo.getTotalRepetitions());
            assertEquals(6, mathUtils.add(1, 5), () -> "should return the right result");

        }

        @Test
        @DisplayName("Add Negative")
        @Tag("Math")
        void testAddNegative() {
            assertEquals(-6, mathUtils.add(-1, -5));
        }
    }

}