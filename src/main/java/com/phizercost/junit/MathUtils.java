package com.phizercost.junit;

public class MathUtils {

    public int add(int a, int b) {
        return a + b;
    }

    public double computeCircleArea(double radius) {
        return Math.PI * radius * radius;
    }

    public double divide(int a, int b) {
        return a / b;
    }
}
